FROM python:slim
MAINTAINER Michael Gliwinski <michael.gliwinski@henderson-group.com>

ENV WAF_VERSION=1.8.11

RUN printf '%s\n' \
    'import os, stat;' \
    'from urllib.request import urlretrieve;' \
    'dst = "/usr/local/bin/waf";' \
    "urlretrieve('https://waf.io/waf-${WAF_VERSION}', dst);" \
    'os.chmod(dst, os.stat(dst).st_mode | stat.S_IEXEC);' \
    |python

# Assume the image will be run like:
# `docker run --rm --volume=$PWD:/src waf ...` so that source to be
# acted upon is in /src.  Waf assumes it is run from and exists in that
# source, so we have to copy it there.
WORKDIR /src
ENTRYPOINT ["bash", "-xc", "cp /usr/local/bin/waf ./ && exec ./waf \"$@\"", "--"]
